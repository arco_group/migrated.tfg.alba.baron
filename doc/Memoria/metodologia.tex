\chapter{Método y Fases de Trabajo}
\label{chap:metodologia}

\drop{E}{n} esta sección se expone la metodología de trabajo seguida para llevar
a  cabo  el   desarrollo  de  este  proyecto,  en  este caso   una  metodología  ágil
\cite{MAgil}.

Las metodologías tradicionales hacen énfasis en el control del desarrollo de los
proyectos  mediante una  rigurosa  definición  de roles  y  de actividades.  Son
efectivas en proyectos de gran tamaño,  sin embargo, este enfoque no resulta ser
tan eficaz  para proyectos  cuyo entorno varía  de forma notable  y en  donde se
exige una gran reducción de los tiempos de desarrollo, pero manteniendo una alta
calidad.

Ante esta situación  muchos equipos de desarrollo dejan a  un lado la ingeniería
del software,  asumiendo los riesgos  que eso conlleva. Las  metodologías ágiles
surgieron  como  respuesta a  ese  problema.  Las  metodologías ágiles  son  una
solución  a medida  para pequeños  proyectos, simplificando  sin renunciar  a lo
esencial para asegurar la calidad del proyecto.

El objetivo  de este tipo  de metodología es permitir  a los equipos  de trabajo
desarrollar  rápidamente software,  respondiendo  a los  diferentes cambios  que
puedan surgir a lo largo del  desarrollo del proyecto. Estas metodologías siguen
<<El Manifiesto Ágil>> \cite{uribe2007manifiesto}, en el cual se valora:

\begin{itemize}
\item Al  individuo y a  las interacciones del equipo  de desarrollo más  que al
  proceso y a las herramientas.
\item El  desarrollo de software  que funciona por  encima de obtener  una buena
  documentación.
\item Colaborar con el cliente sobre la negociación de un contrato.
\item Responder a los cambios más que seguir estrictamente un plan.
\end{itemize}

Existen varias  metodologías ágiles,  cada una  con características  propias que
hacen  hincapié  en ciertos  aspectos  más  específicos.  Algunas de  ellas  son
\cite{ag}:

\begin{itemize}
\item     \emph{SCRUM\footnote{\url{https://www.scrum.org/}}}:     especialmente
  indicada para  proyectos en los que  se suceden cambios en  los requisitos. El
  proceso de desarrollo de software se realiza mediante sprints, iteraciones. El
  resultado de cada sprint debe poder ejecutarse y es mostrado al cliente. Se da
  mucha importancia a las reuniones con el equipo.

\item                                                              \emph{Crystal
    Methodologies\footnote{\url{http://crystalclearmethodology.blogspot.com.es/}}}:
  centrada en las personas que forman el  equipo y en la reducción del número de
  artefactos  producidos. Considera  el  desarrollo de  software  como un  juego
  cooperativo de invención y comunicación entre el equipo, sólo limitado por los
  recursos a utilizar. Es muy importante el equipo de desarrollo, se esfuerza en
  mejorar sus cualidades y en llevar a cabo políticas de trabajo en grupo.

\item \emph{Dynamic Systems Development Method\footnote{\url{www.dsdm.org}}}: es
  un proceso iterativo e incremental en el  que la clave es la cooperación entre
  el  equipo de  desarrollo y  el usuario.  Cuenta con  cinco fases:  estudio de
  viabilidad,  estudio  del negocio,  modelado  de  la funcionalidad,  diseño  y
  construcción e implementación.

\item  \emph{Adaptive Software  Development\footnote{\url{www.adaptivesd.com}}}:
  orientado  a los  componentes software  más que  a las  tareas. Es  un sistema
  iterativo tolerante  a los  cambios con  tres fases  esenciales: especulación,
  colaboración  y  aprendizaje.  Se  inicia  el proyecto  y  se  planifican  las
  características del software;  se desarrollan las características  y se revisa
  su calidad, y  finalmente se entrega al cliente. Para  corregir los errores se
  revisan los componentes y se vuelve a iniciar el ciclo de desarrollo.

\item   \emph{Lean  Development   \footnote{\url{http://www.poppendieck.com/}}}:
  considera riesgos a los cambios, pero si se solucionan adecuadamente se pueden
  convertir  en  oportunidades que  mejoren  la  productividad del  cliente.  Su
  principal  diferencia es  introducir un  mecanismo  para llevar  a cabo  estos
  cambios.

\end{itemize}

Para el desarrollo  de este proyecto se ha decidido  seguir la metodología Scrum
\cite{libroScrum} por  su estrategia de  desarrollo iterativo e  incremental. En
ella, cada iteración  genera un resultado completo, de forma  que los resultados
de las  iteraciones anteriores son  necesarias para realizar las  siguientes. De
esta manera, ofreciendo  cada iteración un resultado completo,  se minimizan los
errores del proyecto general.

\section{SCRUM}

Las características principales de Scrum son:

\begin{itemize}
\item La primera y la última fase  han de tener definidos sus procesos, entradas
  y salidas. El  conocimiento de cómo se  van a llevar a cabo  estos procesos es
  explícito  y se  basa en  hacer  un repositorio  con todas  las actividades  a
  realizar llamado \textit{Backlog System}.
\item El trabajo  se desarrolla en iteraciones de duración  máxima de 4 semanas,
  llamadas  \textit{sprints}. Mediante  reuniones  el equipo  toma
  decisiones  acerca  de qué  incluir  en cada  iteración, con
  estimaciones  de tiempo  para
  finalizar las tareas.
\item  Estos \textit{sprints}  son flexibles  y  no lineales.  El beneficio  que
  aporte al proyecto completo el resultado de cada sprint determina la prioridad
  de éste. Cuando finaliza se realiza una entrega parcial.
\item   En  cada   \textit{sprint}  se   realizan  reuniones   diarias  llamadas
  \textit{Scrum Meetings} en las cuales el desarrollador se pregunta:
\begin{itemize}
\item ¿Qué ha desarrollado desde la última reunión?
\item ¿Qué dificultades concretas tiene el desarrollo de esa tarea?
\item ¿Qué se va a llevar a cabo hasta la próxima reunión?
\end{itemize}

\item El  proyecto se  mantiene abierto  hasta la  fase de  clausura, pudiéndose
  planificar la entrega en cualquiera de  las fases anteriores. De esta forma el
  proyecto se mantiene  abierto a la complejidad  del entorno, al tiempo  o a la
  presión financiera durante el desarrollo de esas fases.
\item Esta metodología está hecha para ser flexible durante el desarrollo de los
  sistemas, de forma que permite cambiar el proyecto y las entregas en cualquier
  punto en el que sea necesario.
\item  Permite  a  los  desarrolladores inventar  soluciones  más  ingeniosas  y
  prácticas a los retos que van surgiendo en el proyecto.
\end{itemize}

Por todos estos motivos es práctico  y eficaz trabajar con esta metodología para
elaborar este proyecto, ya que es muy probable que haya que hacer cambios en los
requisitos en los  plazos de entrega a  lo largo del proyecto. Esto  es debido a
que  no se  puede saber  cómo  responderá el  sistema,  si reconocerá  o no  las
acciones, y  a que  el tiempo  con el que  se cuenta  es limitado.  Usando Scrum
evitamos  los inconvenientes  de las  clásicas  metodologías, en  las cuales  el
proceso de desarrollo está definido por completo desde el principio.

También resulta conveniente que tengamos completas las iteraciones para continuar
con  las siguientes  y que  este  resultado no  pueda  ser alterado,  ya que  lo
necesitamos para llevar a cabo la siguiente iteración.

Otro de los motivos principales por el  que se ha elegido Scrum como metodología
es que permite que cada equipo esté formado solamente por un integrante, aspecto
que se refleja directamente en este proyecto.

\subsection{Fases de SCRUM}
Las diferentes fases  del Scrum consisten en: pre-juego,  juego y
post-juego (ver
Figura~\ref{fig:fases}).

\begin{figure}[!h]
\begin{center}
\includegraphics[scale=0.5]{fases.png}
\caption{Fases de SCRUM \cite{fases}}
\label{fig:fases}
\end{center}
\end{figure}

\begin{itemize}
\item \textbf{Pre-juego}
  \\
  Esta fase se divide en dos: planificación y arquitectura.

  \subitem \textsl{Planificación}: se  define una nueva entrega  basándose en un
  \textit{Backlog}\footnote{Lista de  requisitos priorizada para  desarrollar el
    proyecto.}  que proporciona  el cliente el coste y  cronograma estimados. Si
  un   nuevo  sistema   empieza   a  desarrollarse,   esta   fase  consiste   en
  conceptualización y análisis.

  \subitem \textsl{Arquitectura}:  diseña el plan  en el que los  requisitos del
  \textit{Backlog}  son   implementados.   Esta  fase  incluye   la  creación  o
  modificación de  la arquitectura del sistema  y el diseño de  alto nivel.  Las
  iteraciones se  organizan en base a  la prioridad que tienen  asignada en el
  \textit{Product Backlog list}. Además hay que tener en cuenta las dependencias
  que pueden existir entre las tareas.

\item \textbf{Juego}
  \\
  En  esta  fase  se  lleva  a  cabo  el  desarrollo  de  los  sprints,
  desarrollando la nueva  funcionalidad, es decir, se lleva a  cabo la ejecución
  de cada iteración. Son múltiples los  sprints o ciclos usados para desarrollar
  el  sistema.  Dentro  de un  sprint la  retroalimentación se  obtiene con  las
  reuniones diarias, y el control de la curva de progreso.

  En   cada  iteración   son   llevadas  a   cabo   varias  acciones:

  \subitem \emph{Planificación}: se divide el  trabajo de la iteración en varias
  tareas y se definen los objetivos de estas.

  \subitem \emph{Análisis  de requisitos}: se  determinan los requisitos  que se
  deben conseguir al desarrollar la iteración.

  \subitem \emph{Diseño}:  se diseñan  diagramas esquemáticos para  facilitar la
  comprensión de las acciones y tareas a realizar en cada iteración.

  \subitem \emph{Codificación}: se implementa la funcionalidad de la iteración.

  \subitem  \emph{Revisión}:  se  lleva  a cabo  la  verificación  del  sistema,
  revisando  que no  haya errores.   Además  hay que  asegurarse de  que se  han
  cumplido los objetivos determinados.


\item \textbf{Post-juego}
  \\
  Cuando  se completa  la  iteración,  incluyendo la  documentación  final y  la
  prueba,  se tienen  que exponer  al cliente  los resultados  del desarrollo  y
  expecificar los requisitos cumplidos. Esto debe hacerse en una reunión llamada
  \textit{Sprint Review}.   Gracias a Scrum  el cliente  puede realizar  cambios y
  reajustar el proyecto en el caso de que los resultados mostrados en la reunión
  cumplan sus expectativas \cite{Pamplona}.
\end{itemize}

\subsection{Roles y fases de Scrum}

\subsubsection{Roles Scrum}
Para seguir la  metodología Scrum es necesario definir los  roles que seguirá el
equipo de desarrollo. A continuación se  exponen los diferentes roles que se han
asignado     a   los   diferentes   integrantes   de   este   proyecto   (ver
Cuadro~\ref{tab:roles}).

\begin{table}[hp]
  \centering
  {\small
  \input{tables/roles.tex}
  }
  \caption[Roles de los integrantes del proyecto]
  {Roles de los integrantes del proyecto}
  \label{tab:roles}
\end{table}

Para el caso  particular de este proyecto  no ha sido necesario  recurrir al rol
auxiliar de \textit{manager} ni al de \textit{stakeholder}.

\subsubsection{Fases Scrum}
En este  apartado se describen  las distintas iteraciones  en las que  se divide
este proyecto basándonos en la metodología ágil Scrum.

El desarrollo  de este proyecto se  divide en siete sprints  o iteraciones, cada
uno   de  ellos   aporta  una   nueva  funcionalidad   ejecutable  al   proyecto
principal.  Cada sprint  sigue  el mismo  esquema (ver  Figura~\ref{fig:ciclo}),
siguiendo este  se ha  llevado a cabo  la adaptación del  proyecto PerSe  a este
modelo de trabajo. Las fases son las siguientes:

\begin{figure}[!h]
\begin{center}
\includegraphics[scale=0.6]{ciclo.jpg}
\caption{Ciclo de cada iteración \cite{autentia}}
\label{fig:ciclo}
\end{center}
\end{figure}

\begin{itemize}

\item \textbf{Pre-juego}
  \\
  \subitem  \textsl{Planificación}:  basándose  en los  objetivos  descritos  en los objetivos (véase Capítulo \ref{chap:objetivos}) se debe construir un sistema de reconocimiento de
  acciones  a  partir  de  la  información  recogida  por  los  sensores  de  un
  smartphone.

  Los sprints en Scrum tienen una duración comprendida entre 1 y 4 semanas, esta
  cuantía  va variando  según  la  complejidad de  las  tareas  que implique  la
  iteración. También ha  de tenerse en cuenta que el  desarrollo del proyecto se
  ha llevado a cabo solapado con la  superación de las últimas asignaturas de la
  titulación de Grado  en Ingeniería Informática. Esto afecta  directamente a la
  dedicación que se le ha podido otorgar a la realización del proyecto.

  El Product Backlog List definido por el cliente es el siguiente:

\begin{itemize}

\item \emph{1º  Objetivo}: estudio de los  sensores del Samnsung Galaxy  S4 y de
  cómo interaccionar con ellos.

\item \emph{2º Objetivo}: recogida y etiquetado  de datos con el smartphone y su
  puesta a disposición.

\item \emph{3º  Objetivo}: envío de  los datos  recogidos hasta un  servidor que
  procese éstos.

\item \emph{4º Objetivo}: procesamiento de  los datos utilizando la transformada
  \acs{DWT} y medidas estadísticas.

\item  \emph{5º Objetivo}:  generación  de los  archivos  de entrenamiento  para
  posteriormente utilizar el clasificador SVM.

\item   \emph{6º  Objetivo}: fases  de   entrenamiento  y   clasificación  con
  LIBSVM\footnote{http://www.csie.ntu.edu.tw/~cjlin/libsvm/}.

\item \emph{7º Objetivo}: desarrollo del sistema de validación.

\end{itemize}

\subitem \textsl{Arquitectura}:  el \textit{Product  Backlog} presentado  por el
cliente contiene los objetivos a conseguir, ordenados por prioridad temporal, de
forma que se debe  seguir el orden establecido en esta lista  de trabajo para el
desarrollo del proyecto. Es necesario mantener el orden porque las tareas tienen
dependencia entre  ellas y  es necesario  el resultado de  una para  realizar la
siguiente.  Por  lo tanto  el  \textit{Sprint  Planning}  queda definido  de  la
siguiente manera:

\begin{itemize}
\item     \emph{1º     Iteración}:     desarrollo     de     una     aplicación
  Android\footnote{http://www.android.com/}  para la  recogida de  datos de  los
  sensores en archivos.

\item \emph{2º Iteración}: búsqueda de  voluntarios y realización de las pruebas
  para la recogida de datos con el smartphone para el entrenamiento del sistema.

\item \emph{3º Iteración}:  desarrollo de otra aplicación Android  para el envío
  de los datos recogidos hasta un servidor  que procese éstos y los emita en forma
  de ventana deslizante.

\item \emph{4º  Iteración}: implementación del  servidor que recibe los  datos y
  los procesa. Para procesar los datos  se utiliza la transformada \acs{DWT} y también
  medidas estadísticas.

\item \emph{5º Iteración}: implementación del  módulo que genera los archivos de
  entrenamiento para SVM.

\item  \emph{6º  Iteración}: creación  del  software  que  lleva   a  cabo  el
  entrenamiento y clasificación con el módulo LIBSVM.

\item \emph{7º Iteración}: desarrollo del programa de validación del sistema, el
  cual genera matrices de confusión para ver los resultados.
\end{itemize}

\item \textbf{Juego}
  \\
  Para llevar a cabo el Sprint Planning  se ha recurrido a utilizar el gestor de
  proyectos de  software libre  «Redmine\footnote{http://www.redmine.org/}» (ver
  Figura~\ref{fig:Redmain}), con el  cual se ha organizado el  trabajo en tareas
  para simplificar la coordinación entre los distintos integrantes del equipo.

\begin{figure}[!h]
\begin{center}
\includegraphics[scale=0.6]{Redmain.png}
\caption{Diagrama de Gantt de Readmine}
\label{fig:Redmain}
\end{center}
\end{figure}


\subitem \textbf{Iteración 1: aplicación de recogida de datos de los sensores}

\subsubitem \emph{Planificación}: implementación de una aplicación Android capaz
de leer los valores que ofrecen los sensores del smartphone.

\subsubitem  \emph{Análisis  de requisitos}: la  aplicación  debe almacenar  en
archivos, etiquetados con  la identificación del actor y el  sensor, los valores
que ofrecen los sensores para su posterior procesamiento.

\subsubitem \emph{Diseño}: la  aplicación debe ser simple e  intuitiva, para que
todos los  voluntarios puedan realizar  las pruebas de forma  independiente. Los
valores de los  sensores deben guardarse en archivos ordenados  por eventos (ver
Figura~\ref{fig:Eventos}). Estos  archivos deben  ser almacenados  siguiendo una
jerarquía de directorios (ver Figura~\ref{fig:Jerarquia}).

\begin{figure}[!h]
\begin{center}
\includegraphics[scale=0.6]{Eventos.png}
\caption{Eventos del Acelerómetro}
\label{fig:Eventos}
\end{center}
\end{figure}

\begin{figure}[!h]
\begin{center}
\includegraphics[scale=0.6]{JerarquiaM.jpg}
\caption{Jerarquía de Directorios}
\label{fig:Jerarquia}
\end{center}
\end{figure}

\subsubitem  \emph{Codificación}: se  desarrolla  una  aplicación  Android,  en
lenguaje Java, para  recoger los datos de los sensores  en el smartphone Samsung
Galaxy S4. Esta app ha de tener una interfaz sencilla en la cual el usuario debe
introducir su nombre y elegir una actividad para grabarla.

\subsubitem  \emph{Revisión}: cerciorarse  de que  se guardan  todos los  datos,
aunque  el teléfono  se  encuentre bloqueado  y  que se  guardan  en el  archivo
correcto.

\subitem \textbf{Iteración 2: recogida de datos con el smartphone}

\subsubitem \emph{Planificación}: búsqueda de  voluntarios, toma de contacto con
la  aplicación  y  posteriormente  grabación  de  los  datos.  Idealmente,  cada
voluntario realiza las 14 acciones a predecir en el sistema.

\subsubitem  \emph{Análisis  de  requisitos}:  elaboración e  impresión  de  los
contratos    de   consentimiento    para    la   protección    de   los    datos
personales. Utilización  exclusiva del smartphone  Samsung Galaxy S4  para hacer
las pruebas, de modo que los valores sean semejantes aunque varíe el usuario.

\subsubitem  \emph{Diseño}: las  pruebas han  de hacerse  preferiblemente en  un
entorno apropiado, ya que existen acciones  como: caerse, patada o puñetazo, que
exigen tener algunos materiales para  evitar accidentes (material deportivo como
colchonetas o punching balls).

\subsubitem \emph{Codificación}: se llevan a cabo las pruebas de los voluntarios
y se cumplimentan los formularios de consentimiento.

\subsubitem  \emph{Revisión}:  verificar  que  los datos  se  han  organizado  y
almacenado correctamente  y que todos  los formularios de consentimiento  se han
firmado y completado debidamente.


\subitem \textbf{Iteración  3: aplicación para  el envío de los  datos recogidos
  hasta un servidor}

\subsubitem \emph{Planificación}:  implementación de una aplicación  Android, en
Java, que envía los datos recogidos  en el directorio Sensores hasta un servidor
TCP.

\subsubitem   \emph{Análisis  de   requisitos}:  los   datos  han   de  enviarse
implementando  una ventana  deslizante.   Esta ventana  debe permitir  modificar
fácilmente sus  parámetros de  solapamiento y dimensión,  para poder  ajustar su
precisión al completar el sistema.

\subsubitem \emph{Diseño}: la  aplicación debe ser sencilla y  debe ser flexible
para poderla unir a  la anterior aplicación en un futuro y  hacer que el sistema
envíe los  datos a tiempo  real.  Junto  a los eventos  debe indicar el  tipo de
acción y el tipo de sensor.

\subsubitem  \emph{Codificación}:  se implementa  la  aplicación  en la  que  se
especifican varios  tipos de  eventos, ya  que el smartphone  ofrece 3  tipos de
eventos según el  tipo de sensor: 3  valores, 2 valores o 1  valor. Los sensores
triaxiales ofrecen 3 valores, el GPS ofrece 2 y el resto de sensores 1.

\subsubitem \emph{Revisión}:  verificar que  los datos se  envían correctamente,
que las ventanas  tienen el tamaño indicado y el  solapamiento determinado en el
código.

\subitem \textbf{Iteración 4: servidor que recibe los datos y los procesa con la
  transformada \acs{DWT} y medidas estadísticas.}

\subsubitem \emph{Planificación}: implementación de  un servidor TCP en lenguaje
Python que recibe los datos de la aplicación anterior y los procesa.

\subsubitem  \emph{Análisis de  requisitos}:  los datos  de  los sensores  deben
procesarse en bloques, definidos por las ventanas. Cada ventana es procesada por
la transformada \acs{DWT}, con el objetivo de  eliminar <<ruido>> de los datos como si
de una  señal analógica  se tratase,  de esta  forma, se  eliminan los  picos de
valores y  los datos inservibles. Asimismo, los coeficientes devueltos  por la
transformada \acs{DWT} deben  ser procesados de nuevo, calculando  su media, varianza,
desviación típica, máximo y mínimo.

\subsubitem \emph{Diseño}:  el diseño  del servidor TCP  es sencillo  gracias al
lenguaje Python. Los datos que recibe  son procesados por funciones que permiten
obtener de las  cadenas de texto los  datos que se necesitan, los  valores y los
tipos de sensor y de acción.

\subsubitem \emph{Codificación}: se lleva a  cabo la implementación del servidor
TCP utilizando  paquetes Python tanto  para el servidor  TCP, para la  \acs{DWT}, como
para las medidas estadísticas. De esta forma se reduce la complejidad, el tiempo
y la probabilidad  de errores, ya que es  un software con el que ya  se tiene la
seguridad de un  buen funcionamiento.  El servidor procesa las  cadenas de texto
que recibe y las va convirtiendo  en valores numéricos.  Una vez obtenidos estos
valores se procesan, por  ventanas, en la \acs{DWT} y después  se calculan sus medidas
estadísticas.


\subsubitem \emph{Revisión}: verificar  que los cálculos son correctos  y que se
han tomado los datos del texto correctamente.

\subitem \textbf{Iteración  5: módulo que  genera los archivos  de entrenamiento
  para el clasificador SVM.}

\subsubitem  \emph{Planificación}:  implementación  de   un  módulo  dentro  del
servidor que  almacena los  valores ya  calculados en  el formato  adecuado para
después  poderlos procesar  con LIBSVM  y llevar  a cabo  el entrenamiento  y la
clasificación.

\subsubitem  \emph{Análisis de  requisitos}:  los valores  deben almacenarse  en
ficheros de  forma correcta, para  que pueda  aceptarlos despúes el  programa de
entrenamiento con LIBSVM.

\subsubitem \emph{Diseño}:  este módulo no  debe ser  muy extenso gracias  a las
facilidades que ofrece Python para el manejo de archivos.

\subsubitem  \emph{Codificación}:  se implementa  un  módulo  software capaz  de
escribir en ficheros  organizados por el tipo de sensor,  los valores procesados
de cada ventana junto con la acción a la que pertenecen.

\subsubitem  \emph{Revisión}:  verificar  que  los ficheros  tienen  un  formato
correctos para el clasificador SVM, para  ello es posible utilizar un script que
valida los ficheros e informa de  si son correctos o no (checkdata.py), pudiendo
así corregir fallos.

\subitem \textbf{Iteración 6: software para el entrenamiento y clasificación con
  el módulo LIBSVM.}

\subsubitem   \emph{Planificación}:   implementación   de   los   programas   de
entrenamiento y clasificación.

\subsubitem \emph{Análisis de  requisitos}: el algoritmo debe  entrenar con todo
el dataset para generar los modelos de las acciones.

\subsubitem  \emph{Diseño}:  el  software  debe   leer  todos  los  archivos  de
entrenamiento de un directorio y procesarlos con LIBSVM para generar los modelos
de cada acción para cada sensor. De esta forma obtenemos 9 archivos con modelos,
uno por cada sensor y en cada uno de ellos modelos de 14 acciones.

\subsubitem \emph{Codificación}:  se lleva  a cabo  la implementación  de varios
archivos  en el  lenguaje de  programación Python  para el  procesamiento de  los
archivos de entrenamiento y la generación de los modelos.

\subsubitem  \emph{Revisión}:   verificar  que  los  modelos   se  han  guardado
correctamente y que la salida del programa es normal.


\subitem  \textbf{Iteración 7: programa de validación del sistema.}

\subsubitem \emph{Planificación}: implementación de mecanismos de validación del
sistema     implementando     una     versión     reducida     del     algoritmo
Leave-One-Out\footnote{http://www.cs.cmu.edu/~schneide/tut5/node42.html}
(validación  cruzada)  y  generando  la  matriz  de  confusión  para cada uno de
los sensores utilizados y los  datos obtenidos.

\subsubitem  \emph{Análisis de  requisitos}:  cada  una de  las  pruebas de  los
voluntarios del dataset o conjunto de datos  debe utilizarse primero en la fase de
entrenamiento para obtener un modelo y  después en la fase de clasificación para
ver cómo responde y después almacenar este resultado en la matriz de confusión.

\subsubitem   \emph{Diseño}:    el   sistema    se   valida   con    el   modelo
Leave-One-Out.

\subsubitem \emph{Codificación}: se lleva a cabo la implementación de un archivo
en el lenguaje de  programación Python que prueba cada uno  de los elementos del
dataset y almacena su resultado en 9 matrices de confusión, una por cada tipo de
sensor.

\subsubitem  \emph{Revisión}:  verificar  que   todas  las  pruebas  hayan  sido
procesadas y que las matrices se han generado y almacenado correctamente.

\item \textbf{Post-juego}
  \\
  La fase de post-juego de cada sprint se presenta a continuación:

\subitem   \textbf{Iteración 1: aplicación de recogida de datos de los sensores.}

Una vez terminado el  sprint, el cual fue completado en 3  semanas, se mostró al
cliente  la aplicación  en ejecución  en la  reunión \textit{Sprint  Review}. El
cliente dio el visto  bueno por lo que se pudo pasar  a desarrollar la siguiente
iteración.

\subitem \textbf{Iteración 2:  recogida de datos con el smartphone.}

Esta iteración se llevó  a cabo en un plazo de dos  semanas. Dada la restricción
de  necesitar el  modelo de  móvil concreto  Samsung Galaxy  S4, algunas  de las
pruebas  se  han  llevado  a  cabo  parcialmente,  ya  que  algunos  voluntarios
realizaron  las pruebas  en su  hogar  y no  les  fue posible  completar las  14
acciones.

\subitem \textbf{Iteración  3: aplicación para  el envío de los  datos recogidos
  hasta un  servidor.}

El  desarrollo  de esta  aplicación  se  extendió  durante  4 semanas.  Para  la
implementación  del algoritmo  de ventana  deslizante se  utilizó un  paquete de
software desarrollado en la asignatura Estructura  de Datos, de la titulación de
Ingeniería  Informática (a  extinguir).   Este paquete  contiene estructuras  de
datos con  la jerarquía de  una cola, muy útiles  para realizar el  algoritmo de
ventana deslizante.


\subitem \textbf{Iteración 4: servidor que recibe los datos y los procesa con la
  transformada \acs{DWT} y medidas estadísticas.}

Esta iteración  fue completada en 3  semanas. Se lleva a  cabo la implementación
del       servidor       TCP       utilizando      el       paquete       Python
SocketServer\footnote{https://docs.python.org/2/library/socketserver.html}.   El
servidor  procesa las  cadenas de  texto  que recibe  y las  va convirtiendo  en
valores numéricos. Una vez obtenidos estos valores se procesan, por ventanas, en
la     \acs{DWT}     utilizando      el       paquete      Python      de      ésta,
pywt\footnote{https://packages.debian.org/stable/python/python-pywt},  y después
se  calculan  sus medidas  estadísticas  utilizando  otra biblioteca  de  Python
llamada numpy\footnote{http://www.numpy.org/}.


\subitem \textbf{Iteración  5: módulo que  genera los archivos  de entrenamiento
  para el clasificador SVM.}

Gracias  a la  simplificación  que  ofrece Python  esta  tarea  sólo supuso  una
semana. Este  módulo se reduce  a pocas  líneas de código  en las cuales  se van
escribiendo en 9 ficheros todos los datos para el entrenamiento, un fichero para
cada sensor. En  esta fase también se  incluye la generación de  los archivos de
entrenamiento de todo el dataset.

\subitem \textbf{Iteración 6: software para el entrenamiento y clasificación con
  el módulo LIBSVM.}


Esta iteración  se ha  llevado a  cabo en un  período de  2 semanas,  aunque con
cambios en los  requisitos. La generación de  los modelos con el  dataset de 154
pruebas (154*9=1386  archivos de eventos) tarda  en procesarse en un  equipo con
4GB de RAM y un procesador de 4 núcleos 6 horas. Esto ha provocado cambios en la
siguiente iteración, ya que  no es posible realizar esta tarea  una vez por cada
prueba del dataset.

\subitem \textbf{Iteración 7: programa de validación del sistema.}

Ha  sido imposible  llevar a  cabo la  validación cruzada  ya que  hubieran sido
necesarios  38  días  de  ejecución  continua  en  el  equipo  (6  horas  x  154
pruebas). Por tanto,  se han hecho todas las pruebas  con los modelos generales,
los que SVM crea entrenando todo el dataset.  Esta tarea se ha llevado a cabo en
una semana.

\end{itemize}

\section{Herramientas}
\label{subsection:herramientas}
En este apartado se enumeran las herramientas, tanto hardware como software, que han hecho posible el desarrollo del proyecto PerSe.
\subsection{Lenguajes}
Han sido necesarios varios lenguajes de programación para llevar a cabo este proyecto. A continuación se muestran todos ellos:

\subitem \textbf{Java\footnote{https://www.java.com/}}: lenguaje de programación desarrollado orientado a objetos. Este lenguaje se utiliza para el desarrollo de las apliaciones Android.

\subitem \textbf{Python\footnote{https://www.python.org/}}: lenguaje de programación interpretado, utilizado para la implementación del servidor y de los programas de entrenamiento y pruebas.

\subsection{Medios hardware}

Para el desarrollo de este proyecto se han empleado los siguientes dispositivos:

\begin{itemize}
\item Un equipo portátil Asus K53E para la implementación del proyecto, con las siguientes caracerísticas:
\begin{itemize}
\item Procesador Intel® Core™ i3 2310M
\item Memoria DDR3 1333 MHz SDRAM 4GB
\item 500 GB de Disco Duro
\end{itemize}

\item Un smartphone Wiko Cink Peax 2 para probar las aplicaciones durante su desarrollo.
\item Un smartphone Samsung Galaxy 4 para probar la aplicación durante su desarrollo, durante la etapa de entrenamiento y el resto de pruebas necesarias.
\end{itemize}

\subsection{Medios software}

En el transcurso de este proyecto se han empleado diferentes
herramientas y bibliotecas software. A continuación se detallan todas ellas:

\textbf{Sistemas operativos}:
\begin{itemize}
\item\textbf{Ubuntu\footnote{http://www.ubuntu.com/}}: 13.10 64-bit con kernel 3.11.0-26-generic instalado en el equipo portátil indicado en los medios hardware.

\item\textbf{Android\footnote{http://www.android.com/}}: es el sistema operativo con el que funciona el smartphone Samsung Galaxy S4.
\end{itemize}


\textbf{Documentación}:


\begin{itemize}
\item\textbf{Texmaker\footnote{http://www.xm1math.net/texmaker/}}: para editar tanto la memoria del anteproyecto como la del TFG, así como cualquier otra  documentación  adicional en \LaTeX{} \cite{latex}.

\item\textbf{Gimp\footnote{http://www.gimp.org.es/}}: para editar las imágenes y crear algunos gráficos.

\item\textbf{LibreOffice Draw\footnote{https://es.libreoffice.org/descubre/draw/}}: para crear los gráficos.

\item\textbf{Bibtex\footnote{http://www.bibtex.org/}}: para dar formato a listas de referencias en los documentos escritos en \LaTeX{}.

\end{itemize}



\textbf{Desarrollo}:


\begin{itemize}
\item\textbf{Android \acs{SDK}\footnote{http://developer.android.com/sdk/index.html}}: para el desarrollo de software para Android.

\item\textbf{Python-numpy\footnote{http://www.numpy.org/}}: para el módulo de medidas estadísticas.

\item\textbf{Python-pywt\footnote{http://www.pybytes.com/pywavelets/}}: para procesar los datos de los sensores con la transformada \acs{DWT}.

\item\textbf{SocketServer\footnote{https://docs.python.org/2/library/socketserver.html}}: para implementar el servidor \acs{TCP} que recibe los datos de los sensores.

\item\textbf{Mercurial\footnote{http://mercurial.selenic.com/}}: para el control de versiones y poder compartir el proyecto entre los miembros del equipo usando a su vez un repositorio que se encuentra alojado en Bitbucket, en la cuenta del grupo de investigación de la Escuela Superior de Informática ARCO.
\end{itemize}
