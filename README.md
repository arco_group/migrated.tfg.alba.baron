Este repositorio recoge el Trabajo Fin de Grado «PerSe: Person as a Sensor» de la alumna Alba Barón Rubio. Este proyecto tiene como objetivo utilizar la información que ofrecen los sensores de los smartphones. Esos sensores, proporcionarán información para identificar  las acciones que puede realizar la  persona de forma autónoma, acciones como andar, correr o subir escaleras.

El directorio **doc** almacena el contenido del anteproyecto y de la memoria.

El directorio **src** contiene los diferentes módulos de software implementados durante el desarrollo del proyecto:

*Recogida_Datos*: contiene la app para la recogida de datos y el dataset obtenido.

*Preprocesamiento*: contiene la app para el envío de datos en ventana deslizante hasta el servidor y el propio servidor.

*Entrenamiento*: en el se guardan los programas para el entrenamiento y los datos resultantes del entrenamiento de todo el dataset.

*Validación*: contiene las pruebas individuales para el sistema de validación, los programas para probar el sistema y los que generan las matrices de confusión. Así como los resultados en texto y en forma de matriz de confusión.