package cliente;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

public class ClienteTCP {

	private Socket socket;

	private static final int SERVERPORT = 5000;
	//private static final String SERVER_IP = "172.19.209.253";

	public ClienteTCP() {
		
	}

	public void send_values(String tipo,ArrayList<Double> values, final String ip) {		
		new Thread(new Runnable(){
			@Override
			public void run(){
				try {
					InetAddress serverAddr = InetAddress.getByName(ip);
					socket = new Socket(serverAddr, SERVERPORT);
				} catch (UnknownHostException e1) {
					e1.printStackTrace();
				} catch (IOException e1) {
					e1.printStackTrace();
				}

			}
		}).start();
		
		try {
			String str = tipo + " " + values.toString();
			//String str ="Prueba";
			//PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
			PrintWriter out = new PrintWriter(socket.getOutputStream());
			out.println(str);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}