package eventos;


public abstract class Evento {
	public String tipo;
	protected String tiempo;
		
	public abstract Double getX();
	public abstract void setX(Double x);
	public abstract Double getY();
	public abstract void setY(Double y);
	public abstract Double getZ();
	public abstract void setZ(Double z);
	public abstract Double getA();
	public abstract void setA(Double a);
	public abstract Double getD1();
	public abstract void setD1(Double d1);
	public abstract Double getD2();
	public abstract void setD2(Double d2);
	public abstract Double getD3();
	public abstract void setD3(Double d3);
	public abstract Double getD4();
	public abstract void setD4(Double d4);
	public abstract Double getD5();
	public abstract void setD5(Double d5);
	

	public Evento(String t){
		this.tipo = t;
		this.tiempo = " ";
	}
	
	public Evento(String t, String ti){
		this.tipo=t;
		this.tiempo=ti;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getTiempo() {
		return tiempo;
	}

	public void setTiempo(String tiempo) {
		this.tiempo = tiempo;
	}


	@Override
	public String toString() {
		return "Evento ["+ tipo + ", " + tiempo + "]";
	}
	
	public String valores(){
		return "";
	}
	
}
