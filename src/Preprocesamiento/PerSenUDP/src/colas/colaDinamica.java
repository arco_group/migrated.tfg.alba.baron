package colas;

public class colaDinamica<Elemento> extends Cola<Elemento> implements Cloneable {
    private variableEnlazada<Elemento> primero, ultimo;
    public colaDinamica(){
    primero=null;
    ultimo=null;
    }
    public boolean esVacia(){
        return ultimo==null;
    }
    public void añadir(Elemento e){
        if (esVacia()) primero=ultimo=new variableEnlazada<Elemento>(e);
        else {
            ultimo.setEnlace(new variableEnlazada<Elemento>(e));
            ultimo=ultimo.getEnlace();
        }
    }
    public Elemento primero() throws colaException{
        if (esVacia()) throw new colaException("Error. Cola vacía");
        else return primero.getValor();
        }
    public void quitar() throws colaException{
        if (esVacia()) throw new colaException("Error. Cola vacía");
        else {
            primero=primero.getEnlace();
        if (primero==null) ultimo=null;
        }
    }
    public colaDinamica<Elemento> clone() throws CloneNotSupportedException{
        Cola<Elemento> copia=new colaDinamica<Elemento>();
        Cola<Elemento> aux=new colaDinamica<Elemento>();
        while (!esVacia()){
            aux.añadir(primero());
            copia.añadir(primero());
            quitar();
        }
        while (!aux.esVacia()){
            añadir(aux.primero());
            aux.quitar();
        }
       return (colaDinamica<Elemento>) copia;
    }

    public int tamanio() throws CloneNotSupportedException{
    	int res=0;
    	Cola<Elemento> aux = new colaDinamica<Elemento>();
    	aux =this.clone();
    	if(this.esVacia()) return 0;
    	else{
    		while(!aux.esVacia()){
    			res++;
    			aux.quitar();
    		}
    		return res;
    	}
    }
}//colaDinamica
