package com.main;

import java.io.File;
import colas.Cola;
import colas.colaDinamica;
import eventos.*;

import com.example.proyecto_Android.R;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.app.Activity;


public class SensorActivity extends Activity implements SensorEventListener, LocationListener{
	
	private TextView textViewGrabando = null;
	private EditText enombre = null;
	private EditText eip = null;
	private Spinner spinnerActions = null;
    String n = null;
    String a = null;
    String ip = null;

    File directorio, ruta = Environment.getExternalStorageDirectory();
 
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);           
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.actions_names,android.R.layout.simple_spinner_dropdown_item);
                        
	    setContentView(R.layout.activity_sensor);
	    
	    textViewGrabando = (TextView) findViewById(R.id.Grabando);
	    enombre = (EditText)findViewById(R.id.editTextNombre);	 
	    eip = (EditText)findViewById(R.id.editIP);	    
	    
		spinnerActions = (Spinner) findViewById(R.id.actions);		
	    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	     
	    spinnerActions.setAdapter(adapter);
	    	    
	}
    
    public void on(View view){
    	n = enombre.getText().toString();
    	ip = eip.getText().toString();
	    a = String.valueOf(spinnerActions.getSelectedItem());
	    textViewGrabando.setText(" ");
		VentanaDeslizante vd= new VentanaDeslizante(ruta.getAbsolutePath()+"/Sensores/",n,a,
		5, 1,ip);
		
		try {
			procesarArchivosMet(vd);
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		AñadirEscribir("Enviado");
    }
    
    public void SobreEscribir(String g){
    	textViewGrabando.setText(g);
    }
    
    public void AñadirEscribir(String g){
    	textViewGrabando.setText(this.textViewGrabando.getText()+"\n"+g);
    }
   
    
    @Override
	protected void onResume() {
    	super.onResume();
	}
 
	@Override
	protected void onPause() {
		super.onPause();
		//sensorManager.unregisterListener(this);		
	}
			
	@Override
	public void onProviderDisabled(String provider) {}

	@Override
	public void onProviderEnabled(String provider) {}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {}
	
	@Override
    public void onAccuracyChanged(Sensor arg0, int arg1) {}

	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void onSensorChanged(SensorEvent event) {
		// TODO Auto-generated method stub
		
	}
	
public static void procesarArchivosMet(VentanaDeslizante vd) throws CloneNotSupportedException{
		
		ProcesadorArchivos p = null;
		Cola<Evento> aux = new colaDinamica<Evento>();
		Cola<Evento> ventana = null;
		
		//ACELEROMETRO
		p = new ProcesadorArchivos(vd.dir_acelerometro);
		p.leer();
		aux = p.getEventos().clone();
		ventana = new colaDinamica<Evento>();
						
		if(!aux.esVacia())
			vd.ventana_deslizante(aux,ventana,vd.DIM_VENTANA_ACELEROMETRO,vd.SUPERPOSICION_ACELEROMETRO);
		
		
		//BAROMETRO
		p = new ProcesadorArchivos(vd.dir_barometro);
		p.leer();
		aux = p.getEventos().clone();
		ventana = new colaDinamica<Evento>();
		
		if(!aux.esVacia())
			vd.ventana_deslizante(aux,ventana,vd.DIM_VENTANA_BAROMETRO,vd.SUPERPOSICION_BAROMETRO);
		
		//GIROSCOPIO
		p = new ProcesadorArchivos(vd.dir_giroscopio);
		p.leer();
		aux = p.getEventos().clone();
		ventana = new colaDinamica<Evento>();
		
		if(!aux.esVacia())
			vd.ventana_deslizante(aux,ventana,vd.DIM_VENTANA_GIROSCOPIO,vd.SUPERPOSICION_GIROSCOPIO);	
		
		//GPS
		p = new ProcesadorArchivos(vd.dir_gps);
		p.leer();
		aux = p.getEventos().clone();
		ventana = new colaDinamica<Evento>();
				
		if(!aux.esVacia())
			vd.ventana_deslizante(aux,ventana,vd.DIM_VENTANA_GPS,vd.SUPERPOSICION_GPS);
		
		
		//HUMEDAD
		p = new ProcesadorArchivos(vd.dir_humedad);
		p.leer();
		aux = p.getEventos().clone();
		ventana = new colaDinamica<Evento>();
		
		if(!aux.esVacia())
			vd.ventana_deslizante(aux,ventana,vd.DIM_VENTANA_HUMEDAD,vd.SUPERPOSICION_HUMEDAD);
		
		
		//LUZ
		p = new ProcesadorArchivos(vd.dir_luz);
		p.leer();
		aux = p.getEventos().clone();
		ventana = new colaDinamica<Evento>();
		
		if(!aux.esVacia())
			vd.ventana_deslizante(aux,ventana,vd.DIM_VENTANA_LUZ,vd.SUPERPOSICION_LUZ);
		
		
		//MAGNETICO
		p = new ProcesadorArchivos(vd.dir_magnetico);
		p.leer();
		aux = p.getEventos().clone();
		ventana = new colaDinamica<Evento>();
				
		if(!aux.esVacia())
			vd.ventana_deslizante(aux,ventana,vd.DIM_VENTANA_MAGNETICO,vd.SUPERPOSICION_MAGNETICO);
		
		
		//PROXIMIDAD
		p = new ProcesadorArchivos(vd.dir_proximidad);
		p.leer();
		aux = p.getEventos().clone();
		ventana = new colaDinamica<Evento>();
				
		if(!aux.esVacia())
			vd.ventana_deslizante(aux,ventana,vd.DIM_VENTANA_PROXIMIDAD,vd.SUPERPOSICION_PROXIMIDAD);
		
		
		//TEMPERATURA
		p = new ProcesadorArchivos(vd.dir_temperatura);
		p.leer();
		aux = p.getEventos().clone();
		ventana = new colaDinamica<Evento>();
				
		if(!aux.esVacia())
			vd.ventana_deslizante(aux,ventana,vd.DIM_VENTANA_TEMPERATURA,vd.SUPERPOSICION_TEMPERATURA);
		
	}
}