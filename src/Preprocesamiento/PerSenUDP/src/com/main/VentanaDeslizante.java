package com.main;

import java.util.ArrayList;

import cliente.Cliente;
import eventos.Evento;

import colas.Cola;
import colas.colaDinamica;

public class VentanaDeslizante {
	
	public String raiz;
	
	public String dir_barometro,dir_giroscopio,dir_gps, dir_humedad, dir_luz;
	public String dir_proximidad, dir_magnetico, dir_temperatura, dir_acelerometro,ip;
	
	public int DIM_VENTANA_BAROMETRO, DIM_VENTANA_GIROSCOPIO, DIM_VENTANA_GPS, DIM_VENTANA_HUMEDAD, DIM_VENTANA_LUZ;
	public int DIM_VENTANA_MAGNETICO, DIM_VENTANA_PROXIMIDAD, DIM_VENTANA_TEMPERATURA, DIM_VENTANA_ACELEROMETRO;
	
	public int SUPERPOSICION_BAROMETRO, SUPERPOSICION_GIROSCOPIO, SUPERPOSICION_GPS, SUPERPOSICION_HUMEDAD, SUPERPOSICION_LUZ;
	public int SUPERPOSICION_MAGNETICO, SUPERPOSICION_PROXIMIDAD, SUPERPOSICION_TEMPERATURA, SUPERPOSICION_ACELEROMETRO;
			
	//CONSTRUCTOR COMPLETO
	public VentanaDeslizante(String directorio, String usuario,  String actividad,
			int dIM_VENTANA_BAROMETRO, int dIM_VENTANA_GIROSCOPIO,
			int dIM_VENTANA_GPS, int dIM_VENTANA_HUMEDAD, int dIM_VENTANA_LUZ,
			int dIM_VENTANA_MAGNETICO, int dIM_VENTANA_PROXIMIDAD,
			int dIM_VENTANA_TEMPERATURA, int dIM_VENTANA_ACELEROMETRO,
			int sUPERPOSICION_BAROMETRO, int sUPERPOSICION_GIROSCOPIO,
			int sUPERPOSICION_GPS, int sUPERPOSICION_HUMEDAD,
			int sUPERPOSICION_LUZ, int sUPERPOSICION_MAGNETICO,
			int sUPERPOSICION_PROXIMIDAD, int sUPERPOSICION_TEMPERATURA,
			int sUPERPOSICION_ACELEROMETRO,
			String ip) {
				
		raiz = directorio + usuario + "_" + actividad +"/" + usuario + "_" + actividad + "_";
		dir_barometro = raiz+"Barometro.txt";
		dir_giroscopio = raiz+"Giroscopio.txt";
		dir_gps = raiz+"GPS.txt";
		dir_humedad = raiz+"Humedad.txt";
		dir_luz = raiz+"Luz.txt";
		dir_proximidad = raiz+"Proximidad.txt";
		dir_magnetico = raiz+"Magnetico.txt";
		dir_temperatura = raiz+"Temperatura.txt";
		dir_acelerometro = raiz+"Acelerometro.txt";
		DIM_VENTANA_BAROMETRO = dIM_VENTANA_BAROMETRO;
		DIM_VENTANA_GIROSCOPIO = dIM_VENTANA_GIROSCOPIO;
		DIM_VENTANA_GPS = dIM_VENTANA_GPS;
		DIM_VENTANA_HUMEDAD = dIM_VENTANA_HUMEDAD;
		DIM_VENTANA_LUZ = dIM_VENTANA_LUZ;
		DIM_VENTANA_MAGNETICO = dIM_VENTANA_MAGNETICO;
		DIM_VENTANA_PROXIMIDAD = dIM_VENTANA_PROXIMIDAD;
		DIM_VENTANA_TEMPERATURA = dIM_VENTANA_TEMPERATURA;
		DIM_VENTANA_ACELEROMETRO = dIM_VENTANA_ACELEROMETRO;
		SUPERPOSICION_BAROMETRO = sUPERPOSICION_BAROMETRO;
		SUPERPOSICION_GIROSCOPIO = sUPERPOSICION_GIROSCOPIO;
		SUPERPOSICION_GPS = sUPERPOSICION_GPS;
		SUPERPOSICION_HUMEDAD = sUPERPOSICION_HUMEDAD;
		SUPERPOSICION_LUZ = sUPERPOSICION_LUZ;
		SUPERPOSICION_MAGNETICO = sUPERPOSICION_MAGNETICO;
		SUPERPOSICION_PROXIMIDAD = sUPERPOSICION_PROXIMIDAD;
		SUPERPOSICION_TEMPERATURA = sUPERPOSICION_TEMPERATURA;
		SUPERPOSICION_ACELEROMETRO = sUPERPOSICION_ACELEROMETRO;
		this.ip=ip;
		
	}
	
	//CONSTRUCTOR 2 DIMENSIONES DE VENTANA Y 2 DIMENSIONES DE SUPERPOSICION
	public VentanaDeslizante(String directorio, String usuario,  String actividad, int tam_ventana1,int tam_ventana2,
			int superposicion1, int superposicion2, String ip) {
				
		raiz = directorio + usuario + "_" + actividad +"/" + usuario + "_" + actividad + "_";
		dir_barometro = raiz+"Barometro.txt";
		dir_giroscopio = raiz+"Giroscopio.txt";
		dir_gps = raiz+"GPS.txt";
		dir_humedad = raiz+"Humedad.txt";
		dir_luz = raiz+"Luz.txt";
		dir_proximidad = raiz+"Proximidad.txt";
		dir_magnetico = raiz+"Magnetico.txt";
		dir_temperatura = raiz+"Temperatura.txt";
		dir_acelerometro = raiz+"Acelerometro.txt";
		DIM_VENTANA_BAROMETRO = tam_ventana1;
		DIM_VENTANA_GIROSCOPIO = tam_ventana2;
		DIM_VENTANA_GPS = tam_ventana1;
		DIM_VENTANA_HUMEDAD = tam_ventana1;
		DIM_VENTANA_LUZ = tam_ventana2;
		DIM_VENTANA_MAGNETICO = tam_ventana2;
		DIM_VENTANA_PROXIMIDAD = tam_ventana1;
		DIM_VENTANA_TEMPERATURA = tam_ventana1;
		DIM_VENTANA_ACELEROMETRO = tam_ventana2;
		SUPERPOSICION_BAROMETRO = superposicion1;
		SUPERPOSICION_GIROSCOPIO = superposicion2;
		SUPERPOSICION_GPS = superposicion1;
		SUPERPOSICION_HUMEDAD = superposicion1;
		SUPERPOSICION_LUZ = superposicion2;
		SUPERPOSICION_MAGNETICO = superposicion2;
		SUPERPOSICION_PROXIMIDAD = superposicion1;
		SUPERPOSICION_TEMPERATURA = superposicion1;
		SUPERPOSICION_ACELEROMETRO = superposicion2;
		this.ip=ip;

	}

	//CONSTRUCTOR 1 DIMENSION DE VENTANA Y 1 DIMENSION DE SUPERPOSICION
	public VentanaDeslizante(String directorio, String usuario,  String actividad, int tam_ventana,
			int superposicion, String ip) {
				
		raiz = directorio + usuario + "_" + actividad +"/" + usuario + "_" + actividad + "_";
		dir_barometro = raiz+"Barometro.txt";
		dir_giroscopio = raiz+"Giroscopio.txt";
		dir_gps = raiz+"GPS.txt";
		dir_humedad = raiz+"Humedad.txt";
		dir_luz = raiz+"Luz.txt";
		dir_proximidad = raiz+"Proximidad.txt";
		dir_magnetico = raiz+"Magnetico.txt";
		dir_temperatura = raiz+"Temperatura.txt";
		dir_acelerometro = raiz+"Acelerometro.txt";
		DIM_VENTANA_BAROMETRO = tam_ventana;
		DIM_VENTANA_GIROSCOPIO = tam_ventana;
		DIM_VENTANA_GPS = tam_ventana;
		DIM_VENTANA_HUMEDAD = tam_ventana;
		DIM_VENTANA_LUZ = tam_ventana;
		DIM_VENTANA_MAGNETICO = tam_ventana;
		DIM_VENTANA_PROXIMIDAD = tam_ventana;
		DIM_VENTANA_TEMPERATURA = tam_ventana;
		DIM_VENTANA_ACELEROMETRO = tam_ventana;
		SUPERPOSICION_BAROMETRO = superposicion;
		SUPERPOSICION_GIROSCOPIO = superposicion;
		SUPERPOSICION_GPS = superposicion;
		SUPERPOSICION_HUMEDAD = superposicion;
		SUPERPOSICION_LUZ = superposicion;
		SUPERPOSICION_MAGNETICO = superposicion;
		SUPERPOSICION_PROXIMIDAD = superposicion;
		SUPERPOSICION_TEMPERATURA = superposicion;
		SUPERPOSICION_ACELEROMETRO = superposicion;
		this.ip=ip;

	}
	
	public void ventana_deslizante(Cola<Evento> aux,Cola<Evento> ventana,int tam_ventana, int superposicion) throws CloneNotSupportedException{
		// LLENAMOS LA VENTANA HASTA QUEDARNOS SIN VALORES
		while(!aux.esVacia()){	
			if(ventana.tamanio()<tam_ventana){	// METEMOS LOS PRIMEROS X VALORES
				for(int i=0;i<tam_ventana;i++){			
					if(!aux.esVacia()){
						ventana.añadir(aux.primero());
						aux.quitar();
					}
				}
				
			}else{ 		// REALIZAMOS LOS DESPLAZAMIENTOS DE LA VENTANA
				for(int i=0;i<tam_ventana-superposicion;i++){
					ventana.quitar();
					if(!aux.esVacia()){
						ventana.añadir(aux.primero());
						aux.quitar();
					}
				}
			}
			enviarVentana(ventana, ip);
		}
	}
	
	public void enviarVentana(Cola<Evento> ventana, String ip){
		ArrayList<Double> valores=new ArrayList<Double>();
		Cliente c = new Cliente();	
		valores=getValores(ventana);
		String tipo = ventana.primero().getTipo();
		c.send_values(tipo,valores,ip);
	}

	public static int tipoEntero(String tipo){
		if(tipo.equals("Barometro")|| tipo.equals("Luz")|| tipo.equals("Proximidad") || tipo.equals("Temperatura")|| tipo.equals("Humedad") )
			return 1;
		else if(tipo.equals("GPS"))
			return 2;
		return 3;
	}
	
	public ArrayList<Double> getValores(Cola<Evento> ventana){
		Cola<Evento> aux=new colaDinamica<Evento>();
		ArrayList<Double> valores=new ArrayList<Double>();
		int tipo;
		while (!ventana.esVacia()){
			aux.añadir(ventana.primero());
			tipo = tipoEntero(aux.primero().getTipo());
			switch(tipo){
			case 1:
				valores.add(ventana.primero().getX());
				break;
			case 2:
				valores.add(ventana.primero().getX());
				valores.add(ventana.primero().getY());
				break;
			case 3:
				valores.add(ventana.primero().getX());
				valores.add(ventana.primero().getY());
				valores.add(ventana.primero().getZ());
				break;
			}
			ventana.quitar();
		}
		while (!aux.esVacia()){
			ventana.añadir(aux.primero());
			aux.quitar();
		}
		return valores;
	}
}
