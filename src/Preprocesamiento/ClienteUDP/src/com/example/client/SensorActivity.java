package com.example.client;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import android.support.v7.app.ActionBarActivity;
import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class SensorActivity extends ActionBarActivity {    
	private DatagramSocket s;

	public static final int SERVERPORT = 5000;
    public static final String SERVER_IP = "192.168.1.9";
    public InetAddress serverAddr;
    public TextView Mensaje = null;
    public String resultado = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Mensaje = (TextView) findViewById(R.id.Mensaje);
    }

    public void send_values(View view){
    	try{
    		serverAddr = InetAddress.getByName(SERVER_IP);
    	    s = new DatagramSocket();
    	}catch (UnknownHostException e1){
    		e1.printStackTrace();
    	}catch (IOException e1){
    	    e1.printStackTrace();
    	}
    	ArrayList<Double> values = new ArrayList<Double>();
    	for(int i=0; i<5; i++)
    		values.add(Math.random()*10);
    	new Thread(new Client(values)).start();
        Mensaje.setText(resultado);    	
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.finish:
            	this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    
    public class Client implements Runnable{
        ArrayList<Double> values;
        Client(ArrayList<Double> v) {values=v;}
		public void run(){
			String m= values.toString();
            byte[] message = m.getBytes();
            int msg_length = m.length();
            byte[] buf = new byte[1000];
            DatagramPacket p = new DatagramPacket(message, msg_length,serverAddr, SERVERPORT);
            DatagramPacket res = new DatagramPacket(buf, buf.length);
            try{
                s = new DatagramSocket();
                s.send(p); 
                s.receive(res);
                resultado = new String(res.getData());
            }catch (IOException w){
                w.printStackTrace();
            }
        }
    }    
}
