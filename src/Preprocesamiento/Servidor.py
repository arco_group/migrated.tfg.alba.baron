#!/usr/bin/env python
#-*- coding:utf-8; -*-

import SocketServer
import pywt
import sys
import numpy as np
from svm import *
import unicodedata
import os

class MyTCPHandler(SocketServer.StreamRequestHandler):
	def handle(self):
		while 1:
			try:
				data = self.rfile.readline().strip()
				if not data:
					return
				print data
				accion = data.split(" ",2)[0]
				tipo = data.split(" ",2)[1]
				data = data.split(" ",2)[2]
				s = data[1:-1].replace(" ","")
				values = s.split(",")
				#coeficientes => cA_5, cD_5, cD_4, cD_3, cD_2, cD_1
				coeficientes = pywt.wavedec(values, 'db4', level=5)
				
				#MAX MIN MEDIA DESVIACION VARIANZA
				medidas = []
		
				for i in coeficientes:		
					medidas.append(np.amax(i))
					medidas.append(np.amin(i))
					medidas.append(np.mean(i))
					medidas.append(np.std(i))
					medidas.append(np.var(i))
			
				file = open ("./"+tipo+".txt", "a")
				buf = accion + " "
				x = 1
				for m in medidas:
					buf += str(x) + ":" + str(m) + " "
					x += 1
				buf += "\n"
				file.write(buf.encode ('utf-8'))
				print medidas
				print " "	
				file.close()
			except KeyboardInterrupt:
				sys.exit()

	
if __name__ == "__main__":
	HOST, PORT = sys.argv[1], 5000
	server = SocketServer.TCPServer((HOST, PORT), MyTCPHandler)
	server.serve_forever()
