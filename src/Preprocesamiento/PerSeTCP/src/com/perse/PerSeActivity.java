package com.perse;

import java.io.File;
import java.util.ArrayList;
import java.util.StringTokenizer;

import com.persen.R;

import cliente.ClienteTCP;
import colas.Cola;
import colas.colaDinamica;
import eventos.*;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.app.Activity;


public class PerSeActivity extends Activity{
	
	private static TextView textViewGrabando = null;
	private EditText eip = null;
	private EditText epuerto = null;
	private Spinner spinnerActions = null;
    static String a = null;
    String ip = null;
    int puerto = 0;
    static ClienteTCP c = null;
    String n = null;

    static File directorio, ruta = Environment.getExternalStorageDirectory();
 
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);           
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.actions_names,android.R.layout.simple_spinner_dropdown_item);
                        
	    setContentView(R.layout.activity_sensor);
	    
	    textViewGrabando = (TextView) findViewById(R.id.Grabando);
	    eip = (EditText)findViewById(R.id.editIP);
	    epuerto = (EditText)findViewById(R.id.editPuerto);
		spinnerActions = (Spinner) findViewById(R.id.actions);		
	    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	     
	    spinnerActions.setAdapter(adapter);
    	    
	}
    
    public void conectar(){
    	ip = eip.getText().toString();
    	puerto = Integer.parseInt(epuerto.getText().toString());
	    c=new ClienteTCP(ip,puerto);
	    c.conectar();
    }
    public void desconectar(){
    	c.desconectar();
    }
    public void con(View view){
    	conectar();
		AñadirEscribir("Conexión solicitada");
    }
    public void on(View view){
	    a = String.valueOf(spinnerActions.getSelectedItem());
		VentanaDeslizante vd= new VentanaDeslizante(5, 1);
		
		try {
			procesarArchivos(vd,a);
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
    }
    
    public void todo(View view){
		VentanaDeslizante vd= new VentanaDeslizante(5, 1);
		try {
			procesarArchivos(vd,"Esperar");
			procesarArchivos(vd,"Andar");
			procesarArchivos(vd,"Correr");
			procesarArchivos(vd,"Sentarse");
			procesarArchivos(vd,"Levantarse");
			procesarArchivos(vd,"Tumbarse");
			procesarArchivos(vd,"Caerse");
			procesarArchivos(vd,"SubirEscaleras");
			procesarArchivos(vd,"BajarEscaleras");
			procesarArchivos(vd,"SubirAscensor");
			procesarArchivos(vd,"BajarAscensor");
			procesarArchivos(vd,"Puñetazo");
			procesarArchivos(vd,"Patada");
			procesarArchivos(vd,"Empujon");
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
    }
    
    public void SobreEscribir(String g){
    	textViewGrabando.setText(g);
    }
    
    public static void AñadirEscribir(String g){
    	textViewGrabando.setText(textViewGrabando.getText()+"\n"+g);
    }
    
    @Override
	protected void onResume() {
    	super.onResume();
    	conectar();
	}
 
	@Override
	protected void onPause() {
		super.onPause();
	}
	
	@Override
	protected void onStop(){
		super.onStop();
		c.desconectar();		
	}
	
	public static String accion(String ruta){
		StringTokenizer st = null;		
		st = new StringTokenizer(ruta, "_");
		st.nextToken();
		return st.nextToken();
	}
	
	public static ArrayList<File> listarDirectorio (File f,String accion){
		ArrayList<File> archivosAccion = new ArrayList<File>();
		File[] ficheros = f.listFiles();
		for (int x=0;x<ficheros.length;x++){
			if(accion(ficheros[x].getName()).equalsIgnoreCase(accion)){
				File[] ficheros_texto = ficheros[x].listFiles();
				for (int y=0;y<ficheros_texto.length;y++){
					archivosAccion.add(ficheros_texto[y]);
				}
			}
		}
		return archivosAccion;
	}
	
	
	public static void procesarArchivos(VentanaDeslizante vd, String accion) throws CloneNotSupportedException{
		
		ProcesadorArchivos p = null;
		Cola<Evento> aux = new colaDinamica<Evento>();
		
		ArrayList<File> archivosAccion = new ArrayList<File>();
		File raiz = new File(ruta.getAbsolutePath()+"/Sensores/");
		if (raiz.exists()){
			archivosAccion=listarDirectorio(raiz,accion);
			for(int i=0; i<archivosAccion.size();i++){
				p = new ProcesadorArchivos(archivosAccion.get(i).getAbsolutePath());
				p.leer();
				aux = p.getEventos().clone();						
				if(!aux.esVacia())
					vd.ventana_deslizante(aux,vd.DIM_VENTANA_ACELEROMETRO,vd.SUPERPOSICION_ACELEROMETRO,c,accion);
			}
			AñadirEscribir("Enviado");
		}
		else{
			AñadirEscribir("No hay archivos");
		}
	}
		
}