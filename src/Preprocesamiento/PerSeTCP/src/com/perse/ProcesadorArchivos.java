package com.perse;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;

import android.util.Log;

import colas.Cola;
import colas.colaDinamica;
import eventos.*;

public class ProcesadorArchivos {
	private static String ruta;
	private Cola<Evento> eventos;
	public String[] archivos ;
	
	public ProcesadorArchivos(String r){
		ruta = r;
		eventos=new colaDinamica<Evento>();
	}
	public static String getRuta() {
		return ruta;
	}
	public static void setRuta(String ruta) {
		ProcesadorArchivos.ruta = ruta;
	}
	public Cola<Evento> getEventos() {
		return eventos;
	}
	public void setEventos(Cola<Evento> eventos) {
		this.eventos = eventos;
	}
	public void leer(){
		File f = new File(ruta);
		BufferedReader texto;
		StringTokenizer st = null;		
		String tipo = tipo(ruta);
		int t=tipoEntero(tipo);
		switch (t){
			case 1:
				try {
					texto = new BufferedReader(new FileReader(f));
					while(texto.ready()){
						Evento aux = new Evento1(tipo);
						st = new StringTokenizer(texto.readLine(), " \t");
						aux.setTiempo(st.nextToken());
						st.nextToken();
						aux.setX(Double.parseDouble(st.nextToken()));
						eventos.añadir(aux);
					}
				}catch (IOException e) {
					e.printStackTrace();
				}
				break;
			case 2:
				try {
					texto = new BufferedReader(new FileReader(f));
					while(texto.ready()){
						Evento2 aux = new Evento2(tipo);
						st = new StringTokenizer(texto.readLine(), " \t");
						aux.setTiempo(st.nextToken());
						st.nextToken();
						aux.setX(Double.parseDouble(st.nextToken()));
						st.nextToken();
						aux.setY(Double.parseDouble(st.nextToken()));
						eventos.añadir(aux);
					}
				}catch (IOException e) {
					
					e.printStackTrace();
				}
				break;
			case 3:
				try {
					texto = new BufferedReader(new FileReader(f) );
					while(texto.ready()){
						Evento3 aux = new Evento3(tipo);
						st = new StringTokenizer(texto.readLine(), " \t");
						aux.setTiempo(st.nextToken());
						st.nextToken();
						aux.setX(Double.parseDouble(st.nextToken()));
						st.nextToken();
						aux.setY(Double.parseDouble(st.nextToken()));
						st.nextToken();
						aux.setZ(Double.parseDouble(st.nextToken()));
						eventos.añadir(aux);
					}
				}catch (IOException e) {
					e.printStackTrace();
				}
				break;
			default:
				Log.e("Leer", "Error al leer los archivos");
				break;
		}
		
	}
	
	
	public String tipo (String ruta){
		StringTokenizer st = null;		
		st = new StringTokenizer(ruta, "/");
		String tipo = null;
		while (st.hasMoreTokens()){
			tipo=st.nextToken();
		}
		st = new StringTokenizer(tipo, "_.");
		st.nextToken();
		st.nextToken();
		tipo=st.nextToken();
		return tipo;
	}
	
	public String accion(String ruta){
		StringTokenizer st = null;		
		st = new StringTokenizer(ruta, "_");
		st.nextToken();
		return st.nextToken();
	}
	
	public ArrayList<File> listarDirectorio (File f,String ruta,String accion){
		ArrayList<File> archivosAccion = new ArrayList<File>();
		File[] ficheros = f.listFiles();
		for (int x=0;x<ficheros.length;x++){
			if(accion(ficheros[x].getName()).equalsIgnoreCase(accion)){
				File[] ficheros_texto = ficheros[x].listFiles();
				for (int y=0;y<ficheros_texto.length;y++){
					archivosAccion.add(ficheros_texto[y]);
				}
			}
		}
		return archivosAccion;
	}
	
	public int tipoEntero(String tipo){
		if(tipo.equals("Barometro")|| tipo.equals("Luz")|| tipo.equals("Proximidad") || tipo.equals("Temperatura")|| tipo.equals("Humedad") )
			return 1;
		else if(tipo.equals("GPS"))
			return 2;
		return 3;
	}
	
	public void imprimir(){
		File f = new File(ruta);
		BufferedReader texto;
		try {
			texto = new BufferedReader( new FileReader( f ) );
			String linea;
			while(texto.ready()){
				linea = texto.readLine();
				System.out.println(linea);
			}
		}catch (IOException e) {
			e.printStackTrace();
		}
	}
}
