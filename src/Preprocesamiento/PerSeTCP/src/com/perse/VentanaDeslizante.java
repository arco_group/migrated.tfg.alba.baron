package com.perse;

import java.util.ArrayList;

import android.util.Log;

import cliente.Cliente;
import cliente.ClienteTCP;
import eventos.Evento;

import colas.Cola;
import colas.colaDinamica;

public class VentanaDeslizante {
	
	public int DIM_VENTANA_BAROMETRO, DIM_VENTANA_GIROSCOPIO, DIM_VENTANA_GPS, DIM_VENTANA_HUMEDAD, DIM_VENTANA_LUZ;
	public int DIM_VENTANA_MAGNETICO, DIM_VENTANA_PROXIMIDAD, DIM_VENTANA_TEMPERATURA, DIM_VENTANA_ACELEROMETRO;
	
	public int SUPERPOSICION_BAROMETRO, SUPERPOSICION_GIROSCOPIO, SUPERPOSICION_GPS, SUPERPOSICION_HUMEDAD, SUPERPOSICION_LUZ;
	public int SUPERPOSICION_MAGNETICO, SUPERPOSICION_PROXIMIDAD, SUPERPOSICION_TEMPERATURA, SUPERPOSICION_ACELEROMETRO;
				
	//CONSTRUCTOR COMPLETO
	public VentanaDeslizante(
			int dIM_VENTANA_BAROMETRO, int dIM_VENTANA_GIROSCOPIO,
			int dIM_VENTANA_GPS, int dIM_VENTANA_HUMEDAD, int dIM_VENTANA_LUZ,
			int dIM_VENTANA_MAGNETICO, int dIM_VENTANA_PROXIMIDAD,
			int dIM_VENTANA_TEMPERATURA, int dIM_VENTANA_ACELEROMETRO,
			int sUPERPOSICION_BAROMETRO, int sUPERPOSICION_GIROSCOPIO,
			int sUPERPOSICION_GPS, int sUPERPOSICION_HUMEDAD,
			int sUPERPOSICION_LUZ, int sUPERPOSICION_MAGNETICO,
			int sUPERPOSICION_PROXIMIDAD, int sUPERPOSICION_TEMPERATURA,
			int sUPERPOSICION_ACELEROMETRO) {
				
		DIM_VENTANA_BAROMETRO = dIM_VENTANA_BAROMETRO;
		DIM_VENTANA_GIROSCOPIO = dIM_VENTANA_GIROSCOPIO;
		DIM_VENTANA_GPS = dIM_VENTANA_GPS;
		DIM_VENTANA_HUMEDAD = dIM_VENTANA_HUMEDAD;
		DIM_VENTANA_LUZ = dIM_VENTANA_LUZ;
		DIM_VENTANA_MAGNETICO = dIM_VENTANA_MAGNETICO;
		DIM_VENTANA_PROXIMIDAD = dIM_VENTANA_PROXIMIDAD;
		DIM_VENTANA_TEMPERATURA = dIM_VENTANA_TEMPERATURA;
		DIM_VENTANA_ACELEROMETRO = dIM_VENTANA_ACELEROMETRO;
		SUPERPOSICION_BAROMETRO = sUPERPOSICION_BAROMETRO;
		SUPERPOSICION_GIROSCOPIO = sUPERPOSICION_GIROSCOPIO;
		SUPERPOSICION_GPS = sUPERPOSICION_GPS;
		SUPERPOSICION_HUMEDAD = sUPERPOSICION_HUMEDAD;
		SUPERPOSICION_LUZ = sUPERPOSICION_LUZ;
		SUPERPOSICION_MAGNETICO = sUPERPOSICION_MAGNETICO;
		SUPERPOSICION_PROXIMIDAD = sUPERPOSICION_PROXIMIDAD;
		SUPERPOSICION_TEMPERATURA = sUPERPOSICION_TEMPERATURA;
		SUPERPOSICION_ACELEROMETRO = sUPERPOSICION_ACELEROMETRO;

	}
	
	//CONSTRUCTOR 2 DIMENSIONES DE VENTANA Y 2 DIMENSIONES DE SUPERPOSICION
	public VentanaDeslizante(
			int tam_ventana1,int tam_ventana2,
			int superposicion1, int superposicion2) {
				
		DIM_VENTANA_BAROMETRO = tam_ventana1;
		DIM_VENTANA_GIROSCOPIO = tam_ventana2;
		DIM_VENTANA_GPS = tam_ventana1;
		DIM_VENTANA_HUMEDAD = tam_ventana1;
		DIM_VENTANA_LUZ = tam_ventana2;
		DIM_VENTANA_MAGNETICO = tam_ventana2;
		DIM_VENTANA_PROXIMIDAD = tam_ventana1;
		DIM_VENTANA_TEMPERATURA = tam_ventana1;
		DIM_VENTANA_ACELEROMETRO = tam_ventana2;
		SUPERPOSICION_BAROMETRO = superposicion1;
		SUPERPOSICION_GIROSCOPIO = superposicion2;
		SUPERPOSICION_GPS = superposicion1;
		SUPERPOSICION_HUMEDAD = superposicion1;
		SUPERPOSICION_LUZ = superposicion2;
		SUPERPOSICION_MAGNETICO = superposicion2;
		SUPERPOSICION_PROXIMIDAD = superposicion1;
		SUPERPOSICION_TEMPERATURA = superposicion1;
		SUPERPOSICION_ACELEROMETRO = superposicion2;

	}

	//CONSTRUCTOR 1 DIMENSION DE VENTANA Y 1 DIMENSION DE SUPERPOSICION
	public VentanaDeslizante(
			int tam_ventana,
			int superposicion) {
				
		DIM_VENTANA_BAROMETRO = tam_ventana;
		DIM_VENTANA_GIROSCOPIO = tam_ventana;
		DIM_VENTANA_GPS = tam_ventana;
		DIM_VENTANA_HUMEDAD = tam_ventana;
		DIM_VENTANA_LUZ = tam_ventana;
		DIM_VENTANA_MAGNETICO = tam_ventana;
		DIM_VENTANA_PROXIMIDAD = tam_ventana;
		DIM_VENTANA_TEMPERATURA = tam_ventana;
		DIM_VENTANA_ACELEROMETRO = tam_ventana;
		SUPERPOSICION_BAROMETRO = superposicion;
		SUPERPOSICION_GIROSCOPIO = superposicion;
		SUPERPOSICION_GPS = superposicion;
		SUPERPOSICION_HUMEDAD = superposicion;
		SUPERPOSICION_LUZ = superposicion;
		SUPERPOSICION_MAGNETICO = superposicion;
		SUPERPOSICION_PROXIMIDAD = superposicion;
		SUPERPOSICION_TEMPERATURA = superposicion;
		SUPERPOSICION_ACELEROMETRO = superposicion;

	}
	
	public void ventana_deslizante(Cola<Evento> aux,int tam_ventana, int superposicion, ClienteTCP c, String accion) throws CloneNotSupportedException{
		Cola<Evento> ventana = new colaDinamica<Evento>();
		while(!aux.esVacia()){	
			if(ventana.tamanio()<tam_ventana){
				// Se introducen los primeros valores
				for(int i=0;i<tam_ventana;i++){			
					if(!aux.esVacia()){
						ventana.añadir(aux.primero());
						aux.quitar();
					}
				}
				
			}else{
				// Se realizan los desplazamientos con la superposición elegida
				for(int i=0;i<tam_ventana-superposicion;i++){
					ventana.quitar();
					if(!aux.esVacia()){
						ventana.añadir(aux.primero());
						aux.quitar();
					}
				}
			}
			enviarVentana(ventana,c,accion);
			Log.e(accion,ventana.toString()+"\n\n");
		}
	}
	
	public void enviarVentana(Cola<Evento> ventana, ClienteTCP c, String accion){
		ArrayList<Double> valores=new ArrayList<Double>();	
		valores=getValores(ventana);
		String tipo = ventana.primero().getTipo();
		c.enviar(tipo,accionEntero(accion),valores);
	}

	public static int tipoEntero(String tipo){
		if(tipo.equals("Barometro")|| tipo.equals("Luz")|| tipo.equals("Proximidad") || tipo.equals("Temperatura")|| tipo.equals("Humedad") )
			return 1;
		else if(tipo.equals("GPS"))
			return 2;
		return 3;
	}
	
	public ArrayList<Double> getValores(Cola<Evento> ventana){
		Cola<Evento> aux=new colaDinamica<Evento>();
		ArrayList<Double> valores=new ArrayList<Double>();
		int tipo;
		while (!ventana.esVacia()){
			aux.añadir(ventana.primero());
			tipo = tipoEntero(aux.primero().getTipo());
			switch(tipo){
			case 1:
				valores.add(ventana.primero().getX());
				break;
			case 2:
				valores.add(ventana.primero().getX());
				valores.add(ventana.primero().getY());
				break;
			case 3:
				valores.add(ventana.primero().getX());
				valores.add(ventana.primero().getY());
				valores.add(ventana.primero().getZ());
				break;
			}
			ventana.quitar();
		}
		while (!aux.esVacia()){
			ventana.añadir(aux.primero());
			aux.quitar();
		}
		return valores;
	}
	public int accionEntero(String accion){
		int n=0;
		String acciones[]={"Esperar","Andar","Correr","Sentarse","Levantarse","Tumbarse","Caerse","SubirEscaleras","BajarEscaleras","SubirAscensor","BajarAscensor","Puñetazo","Patada","Empujon"};
		for(int i=0;i<acciones.length;i++){
			if(acciones[i].equalsIgnoreCase(accion)){
				n=i+1;
			}
		}
		return n;
	}
}
