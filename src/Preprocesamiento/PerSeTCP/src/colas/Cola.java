package colas;
    public abstract class Cola<Elemento> implements Cloneable{
    public abstract boolean esVacia();
    public abstract void añadir(Elemento e);
    public abstract Elemento primero() throws colaException;
    public abstract void quitar() throws colaException;
    public abstract int tamanio() throws CloneNotSupportedException;
    @Override public abstract Cola<Elemento> clone() throws CloneNotSupportedException;
    @Override public String toString(){
        String s="";
        if (esVacia())
            s=s+"Está vacía";
        else {
            Cola<Elemento> aux=new colaDinamica<Elemento>();
                while (!esVacia()){
                    aux.añadir(primero());
                    s=s+primero().toString()+"\n";
                    quitar();
                }
                while (!aux.esVacia()){
                    añadir(aux.primero());
                    aux.quitar();
                }
        }
        return s;
    }//toString
    @Override public boolean equals(Object obj) {
        boolean iguales=obj instanceof Cola;
        if (iguales && this!=obj) {
            Cola<Elemento> c=(Cola<Elemento>)obj;
            Cola<Elemento> caux1=new colaDinamica<Elemento>();
            Cola<Elemento> caux2=new colaDinamica<Elemento>();
            while (!esVacia() && !c.esVacia() && iguales){
                iguales=primero().equals(c.primero());
                caux1.añadir(primero());
                quitar();
                caux2.añadir(c.primero());
                c.quitar();
            }
            iguales=(iguales && esVacia() && c.esVacia());
            if (!iguales){
            //completamos el volcado de ambas colas
                while (!esVacia()){
                    caux1.añadir(primero());
                    quitar();
                }
                while (!c.esVacia()){
                    caux2.añadir(c.primero());
                    c.quitar();
                }
            }
            //recuperamos otra vez las dos colas
            while (!caux1.esVacia()){
                añadir(caux1.primero());
                caux1.quitar();
            }
            while (!caux2.esVacia()){
                c.añadir(caux2.primero());
                caux2.quitar();
            }
        }//if principal
       return iguales;
    }//equals
}//Cola

