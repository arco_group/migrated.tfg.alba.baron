package eventos;


public abstract class Evento {
	public String tipo;
	protected String tiempo;
		
	public abstract Double getX();
	public abstract void setX(Double x);
	public abstract Double getY();
	public abstract void setY(Double y);
	public abstract Double getZ();
	public abstract void setZ(Double z);
	

	public Evento(String t){
		this.tipo = t;
		this.tiempo = " ";
	}
	
	public Evento(String t, String ti){
		this.tipo=t;
		this.tiempo=ti;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getTiempo() {
		return tiempo;
	}

	public void setTiempo(String tiempo) {
		this.tiempo = tiempo;
	}


	@Override
	public String toString() {
		return "Evento ["+ tipo + ", " + tiempo + "]";
	}
	
	public String valores(){
		return "";
	}
	
}
