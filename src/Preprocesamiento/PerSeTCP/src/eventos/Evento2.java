package eventos;

public class Evento2 extends Evento{
	protected Double x;
	protected Double y;

	public Evento2(String t) {
		super(t);
		y = 0.0;
	}

	public Evento2(String t, String tiempo1, double x1, double y1){
		super(t, tiempo1);
		x = x1;
		y = y1;
	}
	
	
	public Double getY() {
		return y;
	}

	
	public void setY(Double y) {
		this.y = y;
	}

	@Override
	public String toString() {
		return "Evento2 [" + tipo + ", " + tiempo + ", x=" + x
				+ ", y=" + y + "]";
	}

	@Override
	public
	Double getX() {
		// TODO Auto-generated method stub
		return this.x;
	}

	@Override
	public
	void setX(Double x) {
		// TODO Auto-generated method stub
		this.x=x;
	}

	@Override
	public
	Double getZ() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public
	void setZ(Double z) {
		// TODO Auto-generated method stub
		
	}
}