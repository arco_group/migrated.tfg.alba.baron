package eventos;

public class Evento3 extends Evento{
	protected Double x;
	protected Double y;
	protected Double z;

	public Evento3(String t) {
		super(t);
		x = 0.0;
		y = 0.0;
		z = 0.0;
	}

	public Evento3(String t, String tiempo1, double x1, double y1,double z1){
		super(t, tiempo1);
		x = x1;
		y = y1;
		z = z1;
	}
	
	@Override
	public
	Double getX() {
		return this.x;
	}

	@Override
	public
	void setX(Double x) {
		this.x = x;
	}

	public Double getY() {
		return y;
	}

	public void setY(Double y) {
		this.y = y;
	}

	public Double getZ() {
		return z;
	}

	public void setZ(Double z) {
		this.z = z;
	}

	@Override
	public String toString() {
		return "Evento3 [" + tipo + ", " + tiempo + ", x=" + x
				+ ", y=" + y + ", z=" + z + "]";
	}
}
