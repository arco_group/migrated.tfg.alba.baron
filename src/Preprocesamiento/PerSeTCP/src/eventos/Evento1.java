package eventos;

public class Evento1 extends Evento{
	protected Double x;
	
	public Evento1(String t) {
		super(t);
		x = 0.0;
	}

	public Evento1(String t, String tiempo1, double x1){
		super(t);
		x = x1;
	}
	
	@Override
	public Double getX() {
		return x;
	}

	@Override
	public void setX(Double x) {
		this.x = x;
	}

	@Override
	public String toString() {
		return "Evento1 [" + tipo + ", " + tiempo + ", x=" + x+"]";
	}

	@Override
	public
	Double getY() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public
	void setY(Double y) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public
	Double getZ() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public
	void setZ(Double z) {
		// TODO Auto-generated method stub
		
	}

}
