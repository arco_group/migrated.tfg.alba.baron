package cliente;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;

public class Cliente {    
	private DatagramSocket s;

	public static final int SERVERPORT = 5000;
    //public static final String SERVER_IP = "192.168.0.9";
    public InetAddress serverAddr;
    public String resultado = "";
        
    public Cliente() {
		super();
	}

	public void send_values(String tipo,ArrayList<Double> values, String ip){
    	try{
    		serverAddr = InetAddress.getByName(ip);
    	    s = new DatagramSocket();
    	}catch (UnknownHostException e1){
    		e1.printStackTrace();
    	}catch (IOException e1){
    	    e1.printStackTrace();
    	}
    	new Thread(new Client(tipo,values)).start();    	
    }
    
    public class Client implements Runnable{
    	String tipo;
        ArrayList<Double> values;
        Client(String t, ArrayList<Double> v) {tipo=t;values=v;}
		public void run(){
			String m= tipo + " " + values.toString();
            byte[] message = m.getBytes();
            int msg_length = m.length();
            DatagramPacket p = new DatagramPacket(message, msg_length,serverAddr, SERVERPORT);
            try{
                s = new DatagramSocket();
                s.send(p); 
            }catch (IOException w){
                w.printStackTrace();
            }
        }
    }    
}
