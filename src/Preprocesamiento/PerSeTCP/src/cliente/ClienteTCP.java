package cliente;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;

public class ClienteTCP {

	private Socket socket;

	private static int puerto;
	private static String ip;
	public BufferedWriter out = null;

	public ClienteTCP(String i, int p) {
		ip=i;
		puerto = p;
	}

	public void conectar() {		
		new Thread(new Runnable(){
			@Override
			public void run(){
				try {
					InetAddress serverAddr = InetAddress.getByName(ip);
					socket = new Socket(serverAddr, puerto);
					out = new BufferedWriter (new OutputStreamWriter(socket.getOutputStream()));
				} catch (UnknownHostException e1) {
					e1.printStackTrace();
				} catch (IOException e1) {
					e1.printStackTrace();
				}

			}
		}).start();
	}
	public void enviar(String tipo, int accion, ArrayList<Double> values) {		
		try {
			String str = accion + " " + tipo + " " + values.toString()+ "\n";
			out.write(str);
			out.flush();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void desconectar(){
		try {
			socket.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}