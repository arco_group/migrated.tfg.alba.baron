#! /bin/bash
if [ $# -eq 2 ]; then
	probar=$(expr $1)
	modelos=$(expr $2)
	prueba=$(expr $2)
fi
if [ $# -ne 3 ]; then
	echo "Formato: ./probar <Probar.py> <Modelos> <Prueba>"
	exit 1
fi

archivos=$(ls $modelos)
for fichero in $archivos
 do
   python $probar $modelos/$fichero $prueba/$fichero.txt
 done

