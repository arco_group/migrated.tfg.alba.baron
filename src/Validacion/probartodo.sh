#! /bin/bash
if [ $# -eq 3 ]; then
	pruebas=$(expr $3)
	modelos=$(expr $2)
	probar=$(expr $1)
fi
if [ $# -ne 3 ]; then
	echo "Formato: ./probartodo <Probar.py> <Modelos> <Pruebas>"
	exit 1
fi

carpetas=$(ls $pruebas)
for nombre in $carpetas
 do
   acciones=$(ls  $pruebas/$nombre)
   for numero in $acciones
   	do
	   ficheros=$(ls $pruebas/$nombre/$numero)
	   for fichero in $ficheros
		 do
		 	sensor=$(echo $fichero | cut -d. -f1)
			python $probar $modelos/$sensor $pruebas/$nombre/$numero/$fichero
		 done
	done
 done

