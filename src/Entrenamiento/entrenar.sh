#! /bin/bash
entrenador="./Entrenador.py"
directorio="./Entrenamiento"
if [ $# -eq 2 ]; then
	entrenador=$(expr $1)
	directorio=$(expr $2)
fi
if [ $# -ne 2 -a $# -ne 0 ]; then
	echo "Indique el entrenador y el directorio de las pruebas o no indique nada. Por defecto:\n Entrenador -> ./Entrenador.py\n Archivos de entrenamiento ./Entrenamiento)"
	exit 1
fi

mkdir -p "Modelos"

archivos=$(ls $directorio)
for dir in $archivos
 do
   python $entrenador $directorio/$dir "Modelos"
 done

