from svmutil import *
import sys

fichero = sys.argv[1]
dir_modelos = sys.argv[2]

todo = fichero.split("/")
for palabra in todo:
	sensor = palabra.split(".",1)[0]
y, x = svm_read_problem(sys.argv[1])
prob = svm_problem(y, x)

param = svm_parameter()
param.kernel_type = LINEAR
param.C = 10

modelo=svm_train(prob, param)
svm_save_model(dir_modelos+"/"+sensor, modelo)

