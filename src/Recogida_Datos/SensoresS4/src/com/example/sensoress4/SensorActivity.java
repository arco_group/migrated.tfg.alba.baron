package com.example.sensoress4;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Calendar;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import com.example.sensoress4.R;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Environment;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class SensorActivity extends Activity implements SensorEventListener, LocationListener{
	private SensorManager sensorManager = null;
    private LocationManager locationManager = null;

    private Sensor sensorDeProximidad = null, sensorDeLuz = null, sensorAcelerometro = null, sensorMagnetico = null;
    private Sensor sensorGiroscopio = null, sensorDeTemperatura = null, sensorDeHumedad = null, sensorBarometro = null;
    
    private TextView textViewAcelerometro = null, textViewLuz = null, textViewProximidad = null;
    private TextView textViewMagnetico = null, textViewGiroscopio = null, textViewTemperatura = null;
    private TextView textViewHumedad = null, textViewBarometro = null;
    private TextView textViewGPS = null;
    private String nombre= " ", accion= " ";
    private Spinner spinnerActions = null;
    private EditText enombre = null;
    
    int GRABAR = 0;
    File ruta = Environment.getExternalStorageDirectory();
    OutputStreamWriter oProximidad,oLuz,oAcelerometro,oMagnetico,oGiroscopio,oTemperatura,oHumedad,oBarometro,oGPS;
	Calendar gcalendar = Calendar.getInstance();
	
    
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        sensorDeProximidad = sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY) ;
        sensorDeLuz = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
        sensorAcelerometro = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sensorMagnetico = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        sensorGiroscopio = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        sensorDeTemperatura = sensorManager.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE);
        sensorDeHumedad = sensorManager.getDefaultSensor(Sensor.TYPE_RELATIVE_HUMIDITY);
        sensorBarometro = sensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE);
        
        registrarSensores();
	    
	    setContentView(R.layout.activity_sensor);
	    
	    mostrarSensores();
        
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.actions_names,android.R.layout.simple_spinner_dropdown_item);
        spinnerActions = (Spinner) findViewById(R.id.actions);		
	    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	     
	    spinnerActions.setAdapter(adapter);
	    enombre = (EditText)findViewById(R.id.editTextNombre);
    }
    public void mostrarSensores(){
    	textViewProximidad = (TextView) findViewById(R.id.sensorDeProximidad);
        textViewLuz = (TextView) findViewById(R.id.sensorDeLuz);
        textViewAcelerometro = (TextView) findViewById(R.id.acelerometro);
        textViewMagnetico = (TextView) findViewById(R.id.sensorMagnetico);
        textViewGiroscopio = (TextView) findViewById(R.id.sensorGiroscopio);
        textViewTemperatura = (TextView) findViewById(R.id.sensorDeTemperatura);
        textViewHumedad = (TextView) findViewById(R.id.sensorDeHumedad);
        textViewBarometro = (TextView) findViewById(R.id.sensorBarometro);
        textViewGPS = (TextView) findViewById(R.id.sensorGPS);
    }
    
    @Override
    public void onSensorChanged(SensorEvent arg0) {
	  	synchronized (this){
    		float[] masData;
    		double xP;
			float xA,xL,xM,xT,xG,xH,xp,yA,yM,yG,zA,zM,zG;
    		switch(arg0.sensor.getType()){
    			case Sensor.TYPE_PROXIMITY:
    				masData = arg0.values;
    				if(masData[0]==0){
    					xP=1.0;
    					textViewProximidad.setText("Objeto proximo");
    				}else{
    					xP=0.0;
    					textViewProximidad.setText("Ningun objeto proximo");
    				}
    				if(GRABAR==1)
                    	escribir_externa("\tx: " + xP + "\n",oProximidad);
    			break;
    			case Sensor.TYPE_LIGHT:
    				masData = arg0.values;
    				xL = masData[0];
    				textViewLuz.setText("x: " + xL );
    				if(GRABAR==1)
                    	escribir_externa("\tx: " + xL +"\n",oLuz); 
        		break;
                case Sensor.TYPE_ACCELEROMETER:
                   	masData = arg0.values;
                   	xA = masData[0];
                   	yA = masData[1];
                    zA = masData[2];
                    textViewAcelerometro.setText("x: " + xA + "\ny: "+ yA + "\nz: "+ zA);
                    if(GRABAR==1)
                    	escribir_externa("\tx: " + xA + "\ty: "+ yA + "\tz: "+ zA +"\n",oAcelerometro);
    			break;
                
                case Sensor.TYPE_MAGNETIC_FIELD:
    				masData = arg0.values;
    				xM = masData[0];
                   	yM = masData[1];
                    zM = masData[2];
                    textViewMagnetico.setText("x: " + xM + "\ny: "+yM + "\nz: "+zM);
                    if(GRABAR==1)
                    	escribir_externa("\tx: " + xM + "\ty: "+ yM + "\tz: "+ zM +"\n",oMagnetico);
    			break;
                case Sensor.TYPE_GYROSCOPE:
    				masData = arg0.values;
    				xG = masData[0];
                   	yG = masData[1];
                    zG = masData[2];
                    textViewGiroscopio.setText("x: " + xG + "\ny: "+yG + "\nz: "+zG);
                    if(GRABAR==1)
                    	escribir_externa("\tx: " + xG + "\ty: "+ yG + "\tz: "+ zG +"\n",oGiroscopio);
    			break;
                case Sensor.TYPE_AMBIENT_TEMPERATURE:
    				masData = arg0.values;
    				xT = masData[0];
                    textViewTemperatura.setText("x: " + xT );
                    if(GRABAR==1)
                    	escribir_externa("\tx: " + xT +"\n",oTemperatura);
    			break;
                case Sensor.TYPE_RELATIVE_HUMIDITY:
    				masData = arg0.values;
    				xH = masData[0];
                    textViewHumedad.setText("x: " + xH );
                    if(GRABAR==1)
                    	escribir_externa("\tx: " + xH +"\n",oHumedad);
    			break;
                case Sensor.TYPE_PRESSURE:
    				masData = arg0.values;
    				xp = masData[0];
                    textViewBarometro.setText("x: " + xp );
                    if(GRABAR==1)
                    	escribir_externa("\tx: " + xp +"\n",oBarometro);
    			break;
    			default:
    				Log.e("ErrorSensor", "No se ha encontrado el sensor " + this.toString());
    			break;
    		}
    	}
    }
    
    @Override
	protected void onResume() {
    	super.onResume();
    }
 
	@Override
	protected void onPause() {
		super.onPause();
	}
	
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.sensor, menu);
        return true;
    }
    
    public void iniciar(View view) throws InterruptedException{
    	grabar(0);
    }
    
    public void grabar(int ms) throws InterruptedException{
    	nombre = enombre.getText().toString();
    	accion = String.valueOf(spinnerActions.getSelectedItem());
    	Toast t= Toast.makeText(getApplicationContext(), "Grabando", Toast.LENGTH_LONG);
        t.setGravity(Gravity.CENTER, 0, 0);
    	if(!this.nombre.equalsIgnoreCase("") && !this.nombre.equalsIgnoreCase(" ")){
    		Thread.sleep(ms);
            GRABAR=1;
            try{
            	abrir_ficheros(nombre, accion);
			}
			catch (Exception ex){
			    Log.e("Sensores", "Error al abrir fichero");
			} 
            getWindow().setBackgroundDrawable(new ColorDrawable(0xFF80FFFF));
        	t.show();
        }else{
        	t.setText("Introduzca un Nombre");
        	t.show();
        }
    }
    
    public void parar(View view) {
    	GRABAR=0;
	    try{
		    cerrar_ficheros();
		}
		catch (Exception ex){
		    Log.e("Sensores", "Error al cerrar fichero");
		} 
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
              
            case R.id.MenuIniciar_5:
				try {
					grabar(5000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
            	return true;
                
            case R.id.menu_parar:
            	GRABAR=0;
			    try{
				    cerrar_ficheros();
    			}
    			catch (Exception ex){
    			    Log.e("Sensores", "Error al cerrar fichero");
    			} 
                getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
                return true;
            case R.id.menu_salir:
            	if(GRABAR==1){
            		try{
    				    cerrar_ficheros();
        			}
        			catch (Exception ex){
        			    Log.e("Sensores", "Error al cerrar fichero");
        			} 
            	}
            	finish();
                return true;
                
            default:
                return super.onOptionsItemSelected(item);
        }
    }
        
    public void abrir_ficheros (String nombre, String accion) throws FileNotFoundException{
    	String sensores = ruta.getAbsolutePath()+"/Sensores";
    	String usuario= nombre + "_" + accion;
    	File directorio=new File(sensores);
    	if(!directorio.exists())
    		directorio.mkdirs(); 
    	sensores = sensores+"/"+usuario;
    	directorio=new File(sensores);
    	if(!directorio.exists())
    		directorio.mkdirs();
	    
	    oProximidad = new OutputStreamWriter(new FileOutputStream(new File(sensores, usuario +"_Proximidad.txt")));
	    oLuz = new OutputStreamWriter(new FileOutputStream(new File(sensores, usuario +"_Luz.txt")));
	    oAcelerometro = new OutputStreamWriter(new FileOutputStream(new File(sensores, usuario +"_Acelerometro.txt")));
	    oMagnetico = new OutputStreamWriter(new FileOutputStream(new File(sensores, usuario +"_Magnetico.txt")));
	    oGiroscopio = new OutputStreamWriter(new FileOutputStream(new File(sensores, usuario +"_Giroscopio.txt")));
	    oTemperatura = new OutputStreamWriter(new FileOutputStream(new File(sensores, usuario +"_Temperatura.txt")));
	    oHumedad = new OutputStreamWriter(new FileOutputStream(new File(sensores, usuario +"_Humedad.txt")));
	    oBarometro = new OutputStreamWriter(new FileOutputStream(new File(sensores, usuario +"_Barometro.txt")));
	    oGPS = new OutputStreamWriter(new FileOutputStream(new File(sensores, usuario +"_GPS.txt")));
    }
	
    public void cerrar_ficheros () throws IOException{
	    oProximidad.close();
	    oLuz.close();
	    oAcelerometro.close();
	    oMagnetico.close();
	    oGiroscopio.close();
	    oTemperatura.close();
	    oHumedad.close();
	    oBarometro.close();
	    oGPS.close();   
    }
    
	@SuppressLint("SimpleDateFormat")
	public void escribir_externa(String texto, OutputStreamWriter fout){
		DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
	    if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())){
			try{
			    fout.append(dateFormat.format(new Date())+"\t"+texto);
			}
			catch (Exception ex){
			    Log.e("Escribir", "Error al escribir fichero");
			}
	    }
    }

	private void registrarSensores(){
		locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000 , 10, this);
		sensorManager.registerListener(this, sensorAcelerometro, SensorManager.SENSOR_DELAY_NORMAL);
		sensorManager.registerListener(this, sensorDeProximidad, SensorManager.SENSOR_DELAY_NORMAL);
		sensorManager.registerListener(this, sensorDeLuz, SensorManager.SENSOR_DELAY_NORMAL);
		sensorManager.registerListener(this, sensorMagnetico, SensorManager.SENSOR_DELAY_NORMAL);
		sensorManager.registerListener(this, sensorGiroscopio, SensorManager.SENSOR_DELAY_NORMAL);
		sensorManager.registerListener(this, sensorDeTemperatura, SensorManager.SENSOR_DELAY_NORMAL);
		sensorManager.registerListener(this, sensorDeHumedad, SensorManager.SENSOR_DELAY_NORMAL);
		sensorManager.registerListener(this, sensorBarometro, SensorManager.SENSOR_DELAY_NORMAL);
	}

	@Override
	public void onLocationChanged(Location gps) {
        String texto = " Latitud: " +  gps.getLatitude() + "\tLongitud: " +  gps.getLongitude()+"\n";
        textViewGPS.setText(texto);
        if(GRABAR==1)
        	escribir_externa(texto,oGPS);
	}

	@Override
	public void onProviderDisabled(String provider) {}

	@Override
	public void onProviderEnabled(String provider) {}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {}
	
	@Override
    public void onAccuracyChanged(Sensor arg0, int arg1) {}
    
}